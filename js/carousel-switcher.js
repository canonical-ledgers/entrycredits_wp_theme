$.fn.preload = function() {
    this.each(function(){
        $('<img/>')[0].src = this;
    });
}

$(document).ready(function () {
    var totalAds = Math.min(carousel_posts.length, carousel_backgrounds.length);
    var current = Math.floor(Math.random() * totalAds);

    $(carousel_backgrounds).preload();

    function updateCarouselImage() {
        $('section.head_main').fadeTo(500, 0.7, function () {
            $(this).css('background-image', "url('" + carousel_backgrounds[current] + "')");
        }).fadeTo(300, 1);
        $('section.head_main section.carousel article.frontitem h1.frontitem-title').text(carousel_posts[current].post_title);
        $('section.head_main section.carousel article.frontitem p.frontitem-body').text(carousel_posts[current].post_content);
        var carousel_frontitem = $('section.head_main section.carousel article.frontitem');
        var carousel_frontitem_clone = carousel_frontitem.clone(true);
        carousel_frontitem.before(carousel_frontitem_clone);
        $("." + carousel_frontitem.attr("class") + ":last").remove();
    }

    updateCarouselImage();

    function previousImage () {
        current = current - 1 >= 0 ? current - 1 : totalAds - 1;
        updateCarouselImage();
    }

    function nextImage () {
        current = current + 1 < totalAds ? current + 1 : 0;
        updateCarouselImage();
    }

    var autoLoad = setInterval(nextImage, 10000);

    $("header section.head_main section.carousel i.right.btn").click(function () {
        nextImage();
        clearInterval(autoLoad);
        autoLoad = setInterval(nextImage, 10000);
    });

    $("header section.head_main section.carousel i.left.btn").click(function () {
        previousImage();
        clearInterval(autoLoad);
        autoLoad = setInterval(nextImage, 10000);
    });
});
