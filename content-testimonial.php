<article class="testimonial">
	<h1 class="testimonial-title"><?php the_title(); ?></h1>
	<p class="testimonial-body"><?php the_content(); ?></p>
</article>
