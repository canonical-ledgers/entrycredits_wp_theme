<article class="ad">
	<h1 class="ad-title"><?php the_title(); ?></h1>
	<p class="ad-body"><?php the_content(); ?></p>
</article>
