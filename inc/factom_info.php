<?php
function factomize_info_pane( $wp_customize ) {
        $wp_customize->add_section ("FactomInfo", array(
            "title"      => __("Factom Info","factom_info"),
            "priority"   => 30
        ));
        $wp_customize->add_setting( 'factoid_address', array(
            'default' => '',
            'sanitize_callback' => 'sanitize_text_field',
        ));
        $wp_customize->add_control("factoid_address", array(
            "type"     => "text",
            "label"    => __( "Enter your Factoid Address"),
            "section"  => "FactomInfo",
            "settings" => "factoid_address"
        ));

        $wp_customize->add_setting( 'factomd_endpoint', array(
            'default' => '',
            'sanitize_callback' => 'sanitize_text_field',
        ));
        $wp_customize->add_control("factomd_endpoint", array(
            "type"     => "text",
            "label"    => __( "Enter your Factomd Endpoint"),
            "section"  => "FactomInfo",
            "settings" => "factomd_endpoint"
        ));

        $wp_customize->add_setting( 'factom_walletd_endpoint', array(
            'default' => '',
            'sanitize_callback' => 'sanitize_text_field',
        ));
        $wp_customize->add_control("factom_walletd_endpoint", array(
            "type"     => "text",
            "label"    => __( "Enter your Factom Walletd Endpoint"),
            "section"  => "FactomInfo",
            "settings" => "factom_walletd_endpoint"
        ));
}
add_action("customize_register", "factomize_info_pane");
