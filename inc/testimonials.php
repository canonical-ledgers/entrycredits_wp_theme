<?php
function testimonial_section( $wp_customize ) {
        $wp_customize->add_section ("testimonials", array(
            "title"      => __("Testimonials","testimonials_select"),
            "priority"   => 30
        ));

        $wp_customize->add_setting("testimonial_left_image");
        $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, "testimonial_left_image", array(
            "label"    => __( "Choose image for left testimonial", "left_testimonial_image"),
            "section"  => "testimonials",
            "settings" => "testimonial_left_image",
        )));
        $wp_customize->add_setting( 'testimonial_left_title', array(
            'default' => '',
            'sanitize_callback' => 'sanitize_text_field',
        ));
        $wp_customize->add_control("testimonial_left_title", array(
            "type"     => "text",
            "label"    => __( "Enter your Left Testimonial Title"),
            "section"  => "testimonials",
            "settings" => "testimonial_left_title"
        ));

        $wp_customize->add_setting( 'testimonial_left_body', array(
            'default' => '',
            'sanitize_callback' => 'sanitize_text_field',
        ));
        $wp_customize->add_control("testimonial_left_body", array(
            "type"     => "text",
            "label"    => __( "Enter your Left Testimonial Body"),
            "section"  => "testimonials",
            "settings" => "testimonial_left_body"
        ));

        $wp_customize->add_setting("testimonial_center_image");
        $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, "testimonial_center_image", array(
            "label"    => __( "Choose image for center testimonial", "center_testimonial_image"),
            "section"  => "testimonials",
            "settings" => "testimonial_center_image",
        )));
        $wp_customize->add_setting( 'testimonial_center_title', array(
            'default' => '',
            'sanitize_callback' => 'sanitize_text_field',
        ));
        $wp_customize->add_control("testimonial_center_title", array(
            "type"     => "text",
            "label"    => __( "Enter your Center Testimonial Title"),
            "section"  => "testimonials",
            "settings" => "testimonial_center_title"
        ));

        $wp_customize->add_setting( 'testimonial_center_body', array(
            'default' => '',
            'sanitize_callback' => 'sanitize_text_field',
        ));
        $wp_customize->add_control("testimonial_center_body", array(
            "type"     => "text",
            "label"    => __( "Enter your Center Testimonial Body"),
            "section"  => "testimonials",
            "settings" => "testimonial_center_body"
        ));

        $wp_customize->add_setting("testimonial_right_image");
        $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, "testimonial_right_image", array(
            "label"    => __( "Choose image for right testimonial", "right_testimonial_image"),
            "section"  => "testimonials",
            "settings" => "testimonial_right_image",
        )));
        $wp_customize->add_setting( 'testimonial_right_title', array(
            'default' => '',
            'sanitize_callback' => 'sanitize_text_field',
        ));
        $wp_customize->add_control("testimonial_right_title", array(
            "type"     => "text",
            "label"    => __( "Enter your Right Testimonial Title"),
            "section"  => "testimonials",
            "settings" => "testimonial_right_title"
        ));

        $wp_customize->add_setting( 'testimonial_right_body', array(
            'default' => '',
            'sanitize_callback' => 'sanitize_text_field',
        ));
        $wp_customize->add_control("testimonial_right_body", array(
            "type"     => "text",
            "label"    => __( "Enter your Right Testimonial Body"),
            "section"  => "testimonials",
            "settings" => "testimonial_right_body"
        ));
}
add_action("customize_register", "testimonial_section");
