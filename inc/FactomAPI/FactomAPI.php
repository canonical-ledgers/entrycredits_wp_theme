<?php

// Checks to make sure the entry credit field is right length (52 characters),
// only contains letters and numbers, no i's or o's and starts with "ec" or "EC"
function verifyEntryCreditAddress ($user_input) {
    $verified = preg_match('/^(ec|EC)[1-9ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{50}$/', $user_input);
    return $verified === 1;
}

function buyentrycredit_address($factoid_address, $entrycredit_address, $amount)
{
        if (!verifyEntryCreditAddress($entrycredit_address))
            return false;
        $output = shell_exec("factom-cli buyec -f {$factoid_address} {$entrycredit_address} {$amount}");
        return $output;
}
//
// echo verifyEntryCreditAddress("EC3emTZegtoGuPz3MRA4uC8SU6Up52abqQUEKqW44TppGGAc4Vrq");
//
// echo buyentrycredit_address("FA3GqgeuqX4BYv4V4JcBXij9L1AHtDEqLxprZmGZPjDSPCqiBLK3", "EC3emTZegtoGuPz3MRA4uC8SU6Up52abqQUEKqW44TppGGAc4Vrq", 30);
