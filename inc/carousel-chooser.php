<?php
function carousel_customize_register( $wp_customize ) {
    for ($i = 1; $i < 5; $i++) {
        $wp_customize->add_section ("carousel", array(
            "title"      => __("Carousel","image_selector_{$i}"),
            "priority"   => 30,
        ));
        
        $wp_customize->add_setting("carousel_image_{$i}");
        $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, "carousel_image_{$i}", array(
            "label"    => __( "Upload Ad Carousel Image {$i}", "carousel_{$i}"),
            "section"  => "carousel",
            "settings" => "carousel_image_{$i}",
        )));
    }
}
add_action("customize_register", "carousel_customize_register");
