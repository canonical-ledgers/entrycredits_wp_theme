<article class="post">
	<h1 class="post-title"><?php the_title(); ?></h1>
	<p class="post-body"><?php the_content(); ?></p>
</article>
