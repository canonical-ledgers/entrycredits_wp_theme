<?php get_header(); ?>
<section class="main">
    <section class="testimonial left">
        <div style='background-image:url("<?php echo get_theme_mod("testimonial_left_image");?>");'></div>
        <h1><?php echo get_theme_mod("testimonial_left_title");?></h1>
        <p><?php echo get_theme_mod("testimonial_left_body");?></p>
    </section>
    <section class="testimonial center">
        <div style='background-image:url("<?php echo get_theme_mod("testimonial_center_image");?>");'></div>
        <h1><?php echo get_theme_mod("testimonial_center_title");?></h1>
        <p><?php echo get_theme_mod("testimonial_center_body");?></p>
    </section>
    <section class="testimonial right">
        <div style='background-image:url("<?php echo get_theme_mod("testimonial_right_image");?>");'></div>
        <h1><?php echo get_theme_mod("testimonial_right_title");?></h1>
        <p><?php echo get_theme_mod("testimonial_right_body");?></p>
    </section>
</section>
<?php get_footer(); ?>
