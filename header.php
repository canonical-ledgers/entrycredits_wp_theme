<html lang="en" dir="ltr">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title><?php echo get_bloginfo('name') . " | " . get_bloginfo('description');?></title>
        <link rel="icon" href="<?php echo get_bloginfo('template_directory'); ?>/img/entrycredits_logo.png">
        <link rel="stylesheet" href="<?php echo get_bloginfo('template_directory'); ?>/css/styles.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Oswald">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
        <?php wp_head(); ?>
        <script type="text/javascript">
            var carousel_posts = JSON.parse(`<?php echo json_encode(get_posts(array('category' => 5)));?>`);
            var carousel_backgrounds = <?php
            $backgrounds = array();
            for ($i = 1; $i < 5; $i++) {
                if (get_theme_mod("carousel_image_{$i}"))
                    $backgrounds[] = get_theme_mod("carousel_image_{$i}");
            }
            echo json_encode($backgrounds, 1); ?>;
        </script>
        <script src="<?php echo get_bloginfo('template_directory'); ?>/js/carousel-switcher.js" charset="utf-8"></script>
    </head>
    <body>
        <header>
            <!-- <section class="head">
                <h1><?php echo get_bloginfo('name');?></h1>
                <h2><?php echo get_bloginfo('description');?></h2>
            </section> -->
            <section class="head_main">
                <nav>
                        <div class="logo"><h1><?php echo get_bloginfo('name');?></h1></div>
                        <?php wp_nav_menu(array("menu" => 'navbar-left', "container" => '', "menu_class" => 'left')); ?>
                        <?php wp_nav_menu(array("menu" => 'navbar-right', "container" => '', "menu_class" => 'right')); ?>
                </nav>
                <?php get_template_part( 'header', 'carousel' ); ?>
            </section>
        </header>
